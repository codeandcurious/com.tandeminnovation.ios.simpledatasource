//
//  FirstCollectionViewCell.swift
//  SimpleDataSource_Example
//
//  Created by Eduardo Dias on 23/11/2018.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import SimpleDataSource

class FirstCollectionViewCell: SimpleCollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Lifecycle
    override func setup<T>(with item: T) where T : FirstItemWrapper {
        self.titleLabel.text = item.title
    }
}
