//
//  ViewController.swift
//  SimpleDataSource
//
//  Created by Eduardo Dias on 11/23/2018.
//  Copyright (c) 2018 Eduardo Dias. All rights reserved.
//

import SimpleDataSource
import UIKit

class ViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    private var simpleDataSource = SimpleCollectionViewDataSource()
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for counter in 0...29 {
            let newFirstItem = FirstItemWrapper(title: "1_\(counter)", data: "", reuseIdentifier: "FirstCollectionViewCell", nibName: "FirstCollectionViewCell")
            self.simpleDataSource.dataSource.append(newFirstItem)
        }
        
        for counter in 0...29 {
            let newSecondItem = SecondItemWrapper(title: "2_\(counter)", data: "", reuseIdentifier: "SecondCollectionViewCell", nibName: "SecondCollectionViewCell")
            self.simpleDataSource.dataSource.append(newSecondItem)
        }
        
        self.collectionView.dataSource = self.simpleDataSource
        
        self.simpleDataSource.dataSourceTypes.forEach({
            self.collectionView.register($0.nib, forCellWithReuseIdentifier: $0.reuseIdentifier)
        })
        
        self.collectionView.reloadData()
    }
}

