//
//  SecondItemWrapper.swift
//  SimpleDataSource_Example
//
//  Created by Eduardo Dias on 23/11/2018.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import SimpleDataSource

class SecondItemWrapper: SimpleCollectionViewItemWrapper {
    
    // MARK: - Properties
    var title: String
    
    // MARK: - Lifecycle
    init(title: String, data: Any, reuseIdentifier: String, nibName: String) {
        self.title = title
        super.init(data: data, reuseIdentifier: reuseIdentifier, nibName: nibName)
    }
}
