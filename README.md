# SimpleDataSource

[![Version](https://img.shields.io/cocoapods/v/SimpleDataSource.svg?style=flat)](https://cocoapods.org/pods/SimpleDataSource)
[![License](https://img.shields.io/cocoapods/l/SimpleDataSource.svg?style=flat)](https://cocoapods.org/pods/SimpleDataSource)
[![Platform](https://img.shields.io/cocoapods/p/SimpleDataSource.svg?style=flat)](https://cocoapods.org/pods/SimpleDataSource)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SimpleDataSource is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SimpleDataSource', :git => 'https://bitbucket.org/codeandcurious/com.tandeminnovation.ios.simpledatasource.git', :branch => 'master', :tag => '1.0.0'
```

## Author

Tandem Innovation, eduardo.dias@nextreality.com

## License

SimpleDataSource is available under the MIT license. See the LICENSE file for more info.
