//
//  SimpleCollectionViewItemWrapper.swift
//  StlDataSource
//
//  Created by Eduardo Dias on 22/11/2018.
//  Copyright © 2018 Joao Pires. All rights reserved.
//

import UIKit

/// The base class for a collection view Item.
open class SimpleCollectionViewItemWrapper {
    
    // MARK: - Properties
    public var data: Any
    public var reuseIdentifier: String
    public var nib: UINib
    public var nibName: String
    
    // MARK: - Lifecycle
    public init(data: Any, reuseIdentifier: String, nibName: String) {
        self.data = data
        self.reuseIdentifier = reuseIdentifier
        self.nibName = nibName
        self.nib = UINib(nibName: nibName, bundle: Bundle.main)
    }
}
