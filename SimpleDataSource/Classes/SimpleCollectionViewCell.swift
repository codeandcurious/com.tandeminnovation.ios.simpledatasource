//
//  SimpleCollectionViewCell.swift
//  StlDataSource
//
//  Created by Eduardo Dias on 22/11/2018.
//  Copyright © 2018 Joao Pires. All rights reserved.
//

import UIKit

/**
 To be used with the Simple Item Wrapper. Set the reuseIdentifier to the one you specify in the ItemWrapper, along with the nibName.
 YOU MUST override SimpleCollectionViewCell's setup method. This will give you access to the item that contains the data for the cell.
 */
open class SimpleCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Lifecycle
    /**
     A SimpleCollectionViewCell subclass must implement this method to configure the cell with the relevant item. Don't call super or else a fatalError will be triggered.
     
     - Parameter withItem: the item to configure the cell with
     */
    open func setup<T: SimpleCollectionViewItemWrapper>(with: T) {
        fatalError(#function + " must be implemented in subclass")
    }
}
