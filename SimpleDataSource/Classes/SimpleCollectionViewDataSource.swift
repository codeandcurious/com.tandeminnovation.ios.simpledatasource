//
//  SimpleCollectionViewDataSource.swift
//  onpocket
//
//  Created by Joao Pires on 11/9/18.
//  Copyright © 2018 TandemInnovation. All rights reserved.
//

import UIKit

public class SimpleCollectionViewDataSource: NSObject {
    
    // MARK: - Custom types
    /// Specifies the nib and reuseIdentifier for all UICollectionViewCells
    public struct SourceType: Hashable {
        
        // MARK: - Properties
        public var nib: UINib
        public var reuseIdentifier: String
        public var hashValue: Int {
            return self.nib.hashValue ^ self.reuseIdentifier.hashValue
        }
    }
    
    // MARK: - Properties
    public var dataSource = [SimpleCollectionViewItemWrapper]() {
        didSet {
            self.dataSourceTypes.removeAll()
            self.dataSource.forEach {
                self.dataSourceTypes.insert(SimpleCollectionViewDataSource.SourceType(nib: $0.nib, reuseIdentifier: $0.reuseIdentifier))
            }
        }
    }
    public private(set) var dataSourceTypes = Set<SimpleCollectionViewDataSource.SourceType>()
}

// MARK: - UICollectionViewDataSource
extension SimpleCollectionViewDataSource: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataSource.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.dataSource[indexPath.row]
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: item.reuseIdentifier, for: indexPath) as! SimpleCollectionViewCell
        itemCell.setup(with: item)
        
        return itemCell
    }
}
